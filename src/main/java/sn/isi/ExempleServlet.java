package sn.isi;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/exemple")
public class ExempleServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().println(
                "<html><body>" +
                "<h1 style='color:blue;'>Bienvenu au cours</h1>" +
                        "<ul><li>Menu1</li>" +
                        "<li>Menu2</li>" +
                        "<li>Menu3</li>" +
                        "<li>Menu4</li>" +
                        "<li>Menu5</li>" +
                        "<li>Menu6</li>" +
                        "</ul>" +
                "</body>" +
                "</html>"
        );
    }
}
